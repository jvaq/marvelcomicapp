package backhero.marvelapp

import android.app.Application
import backhero.marvelapp.handlers.DataHandler

/**
 * Created by javieralvarez on 27/1/18.
 */
class MarvelApp : Application() {

    override fun onCreate() {
        super.onCreate()
        DataHandler.init("https://gateway.marvel.com:443/v1/public/")
    }
}