package backhero.marvelapp.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by javieralvarez on 27/1/18.
 */
 data class Character (
    @SerializedName("available")
     var available: Long? = null,
    @SerializedName("collectionURI")
     var collectionURI: String? = null,
    @SerializedName("items")
     var items: List<Item>? = null,
    @SerializedName("returned")
     var returned: Long? = null
    )