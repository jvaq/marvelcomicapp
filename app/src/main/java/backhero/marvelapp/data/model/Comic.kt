package backhero.marvelapp.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by javieralvarez on 27/1/18.
 */
data class Comic (
    @SerializedName("characters")
     var characters: Character? = null,
    @SerializedName("creators")
     var creators: Creators? = null,
    @SerializedName("description")
     var description: String? = null,
    @SerializedName("id")
     var id: Long? = null,
    @SerializedName("images")
     var images: List<MImages>? = null,
    @SerializedName("prices")
     var prices: List<Price>? = null,
    @SerializedName("title")
     var title: String? = null
)


