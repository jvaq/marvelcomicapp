package backhero.marvelapp.data.model

/**
 * Created by javieralvarez on 27/1/18.
 */
class Data<T> ( var offset: Int?,
                var limit : Int?,
                var count : Int?,
                var results : List<T>?)