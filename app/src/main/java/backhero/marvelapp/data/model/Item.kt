package backhero.marvelapp.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by javieralvarez on 27/1/18.
 */
data class Item(@SerializedName("name")
                var name: String? = null,
                @SerializedName("resourceURI")
                var resourceURI: String? = null,
                @SerializedName("type")
                var type: String? = null,
                @SerializedName("role")
                var role: String? = null)
