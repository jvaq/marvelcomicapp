package backhero.marvelapp.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by javieralvarez on 27/1/18.
 */
data class MImages (@SerializedName("path")
                     var path: String? = null,
                    @SerializedName("extension")
                     var extension: String? = null)
