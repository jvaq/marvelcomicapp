package backhero.marvelapp.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by javieralvarez on 27/1/18.
 */
data class Price(@SerializedName("price")
                  var price: Double? = null,
                 @SerializedName("type")
                  var type: String? = null)