package backhero.marvelapp.data.model

/**
 * Created by javieralvarez on 27/1/18.
 */
data class Response<T> (
        var code: Int?,
        var data : Data<T>? )