package backhero.marvelapp.data.network.base

/**
 * Created by javieralvarez on 27/1/18.
 */
open class BaseRetrofitService<T>(protected var service: T)
