package backhero.marvelapp.data.network.base

import org.apache.commons.codec.binary.Hex
import org.apache.commons.codec.digest.DigestUtils


/**
 * Created by javieralvarez on 27/1/18.
 */
class HashGenerator {
    companion object {

        fun  generateHash(ts: Int): String {
            return String(Hex.encodeHex(DigestUtils.md5(ts.toString() + NetworkConstans.PRIVATE_API_KEY + NetworkConstans.PUBLIC_API_KEY)))
        }
    }
}
