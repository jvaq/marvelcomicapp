package backhero.marvelapp.data.network.handler

import backhero.marvelapp.data.model.Comic
import backhero.marvelapp.data.model.Response
import backhero.marvelapp.data.network.base.BaseRetrofitService
import backhero.marvelapp.data.network.base.HashGenerator
import backhero.marvelapp.data.network.base.NetworkConstans
import backhero.marvelapp.data.network.interfaces.ComicHandlerI
import backhero.marvelapp.data.network.services.ComicService
import io.reactivex.Observable

/**
 * Created by javieralvarez on 27/1/18.
 */
class ComicHandler (service : ComicService) : ComicHandlerI, BaseRetrofitService<ComicService>(service)  {

    override fun getComicById(comicId: String): Observable<Response<Comic>> {
        return service.getComicById(comicId,1,NetworkConstans.PUBLIC_API_KEY,HashGenerator.generateHash(1))
        }

    override fun getComicsByCharacter(characterId: String, offset: Int): Observable<Response<Comic>> {
         return service.getComicsByCharacter(characterId,offset,1,NetworkConstans.PUBLIC_API_KEY,HashGenerator.generateHash(1))
    }
}