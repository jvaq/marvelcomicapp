package backhero.marvelapp.data.network.interfaces

import backhero.marvelapp.data.model.Comic
import backhero.marvelapp.data.model.Response
import io.reactivex.Observable

/**
 * Created by javieralvarez on 27/1/18.
 */
interface ComicHandlerI {

    fun getComicsByCharacter(characterId : String, offset: Int) : Observable<Response<Comic>>

    fun getComicById(comicId : String) : Observable<Response<Comic>>

}