package backhero.marvelapp.data.network.services

import backhero.marvelapp.data.model.Comic
import backhero.marvelapp.data.model.Response
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by javieralvarez on 27/1/18.
 */
interface ComicService {

    @GET("characters/{id}/comics")
    fun getComicsByCharacter(@Path("id") characterId : String, @Query("offset") offset : Int,
                         @Query("ts") ts: Int,@Query("apikey") apikey : String,
                         @Query("hash") hash : String) : Observable<Response<Comic>>


    @GET("comics/{id}")
    fun getComicById(@Path("id") comicId : String,
                     @Query("ts") ts: Int,@Query("apikey") apikey : String,
                     @Query("hash") hash : String) : Observable<Response<Comic>>

}