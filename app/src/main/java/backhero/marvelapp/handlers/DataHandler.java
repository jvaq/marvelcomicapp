package backhero.marvelapp.handlers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import backhero.marvelapp.data.network.base.ServiceGenerator;
import backhero.marvelapp.data.network.handler.ComicHandler;
import backhero.marvelapp.data.network.interfaces.ComicHandlerI;
import backhero.marvelapp.data.network.services.ComicService;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by javieralvarez on 27/1/18.
 */

public class DataHandler {
    private static DataHandler instance;
    public static DataHandler getInstance() {
        return instance;
    }

    private ComicHandlerI comicHandler;

    public static void init(String environment) {
        if (instance == null)
            instance = new DataHandler(environment);
    }


    private DataHandler(String environment) {


        OkHttpClient httpClient = new OkHttpClient.Builder()
                .build();

        Gson gson = new GsonBuilder().create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient)
                .baseUrl(environment)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceGenerator serviceGenerator = new ServiceGenerator(retrofit);
        comicHandler = new ComicHandler(serviceGenerator.createService(ComicService.class));

    }

    public ComicHandlerI getComicHandler() {
        return comicHandler;
    }
}
