package backhero.marvelapp.handlers

import backhero.marvelapp.ui.screens.detail.ComicDetailActivity
import backhero.marvelapp.ui.screens.main.MainActivity

/**
 * Created by javieralvarez on 28/1/18.
 */
class NavigationHandler {

    companion object {

        fun getActivityDestination(destination: String): Class<*>? {

            when (destination) {
                ActivityDestinations.HOME -> return MainActivity::class.java
                ActivityDestinations.COMIC_DETAIL -> return ComicDetailActivity::class.java
            }
            return null
        }

    }

    object ActivityDestinations {
        const val HOME = "home"
        const val COMIC_DETAIL = "comic_detail"
    }

}