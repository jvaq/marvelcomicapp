package backhero.marvelapp.ui.base.mvp

import android.support.annotation.CallSuper
import java.util.ArrayList

/**
 * Created by javieralvarez on 27/1/18.
 */
abstract class BaseInteractor{

    private val calls : ArrayList<Any> = arrayListOf()

    @CallSuper
    fun onDestroy() {
    }
}