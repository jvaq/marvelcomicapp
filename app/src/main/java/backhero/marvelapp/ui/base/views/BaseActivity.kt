package backhero.marvelapp.ui.base.views

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import android.view.View

/**
 * Created by javieralvarez on 27/1/18.
 */
abstract class BaseActivity : AppCompatActivity() {

    @get:LayoutRes
    protected abstract val layout: Int
    protected var bundle : Bundle? = null

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(layout)
        bundle = intent.extras
        bindViews()
        loadData()
    }

    override fun setContentView(view: View) {
        throw IllegalStateException("Method not allowed")
    }

    override fun setContentView(layoutResID: Int) {
        throw IllegalStateException("Method not allowed")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    override fun onPostResume() {
        super.onPostResume()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    /**
     * Called by parent to notify that views can be bind
     * views
     */
    protected abstract fun bindViews()


    /**
     * Called by parent to notify that it can load screen related data
     */
    protected abstract fun loadData()
}