package backhero.marvelapp.ui.screens.detail

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import backhero.marvelapp.MarvelApp
import backhero.marvelapp.R
import backhero.marvelapp.data.model.Comic
import backhero.marvelapp.data.model.Item
import backhero.marvelapp.ui.base.views.BaseActivity
import backhero.marvelapp.ui.screens.views.MarvelNavbar
import backhero.marvelapp.utils.extensions.loadImage
import backhero.marvelapp.utils.images.ImageGenerator

import kotlinx.android.synthetic.main.activity_comic_detail.*

class ComicDetailActivity : BaseActivity(), ComicDetailView {


    override val layout: Int = R.layout.activity_comic_detail
    lateinit var navbar: MarvelNavbar
    lateinit var ivComic: ImageView
    lateinit var tvTitle: TextView
    lateinit var tvDescription: TextView
    lateinit var rvItems : RecyclerView
    val presenter = ComicDetailPresenter(this)
    var adapter = ItemAdapter()


    override fun bindViews() {
        navbar = nv_detail
        ivComic = iv_image
        tvTitle = tv_title_detail
        tvDescription = tv_description
        rvItems = rv_items
    }

    override fun loadData() {
        presenter.init()
        navbar.onBackPressed(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                onBackPressed()
            }
        })
        bundle?.getString("id")?.apply {
            presenter.getComic(this)
        }
        val linearLayout = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        rvItems.layoutManager = linearLayout
        rvItems.adapter = adapter

    }

    override fun showComic(comic: Comic) {
        ivComic.loadImage(ImageGenerator.generateLandscapeImageUrl(comic.images))
        tvTitle.text = comic.title
        tvDescription.text = comic.description
        tv_characters.visibility = View.VISIBLE
        var items = mutableListOf<Item>()
        comic.characters?.items?.apply {
            items.addAll(this)
        }
        comic.creators?.items?.apply {
            items.addAll(this)

        }
        adapter.addItems(items)

    }

    override fun showBackButton() {
        navbar.showBackButton(true)
    }


}
