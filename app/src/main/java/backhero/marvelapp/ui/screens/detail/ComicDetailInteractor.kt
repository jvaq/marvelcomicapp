package backhero.marvelapp.ui.screens.detail

import backhero.marvelapp.data.model.Comic
import backhero.marvelapp.data.model.Response
import backhero.marvelapp.handlers.DataHandler
import backhero.marvelapp.handlers.NavigationHandler
import backhero.marvelapp.ui.base.mvp.BaseInteractor
import io.reactivex.Observable

/**
 * Created by javieralvarez on 28/1/18.
 */
class ComicDetailInteractor : BaseInteractor() {

    fun getComicById(comicId : String) : Observable<Response<Comic>> {
       return DataHandler.getInstance().comicHandler.getComicById(comicId)
    }
}