package backhero.marvelapp.ui.screens.detail

import android.util.Log
import backhero.marvelapp.ui.base.mvp.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by javieralvarez on 28/1/18.
 */
class ComicDetailPresenter(mView: ComicDetailView) : BasePresenter<ComicDetailView, ComicDetailInteractor>(mView) {

    override val interactor: ComicDetailInteractor = ComicDetailInteractor()

    override fun init() {
        mView?.showBackButton()
    }

    fun getComic(id: String) {

        interactor.getComicById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ comic ->
                    comic.data?.results?.first()?.apply { mView?.showComic(this) }
                },
                        { error -> Log.d("error", error.message) }
                )
    }
}