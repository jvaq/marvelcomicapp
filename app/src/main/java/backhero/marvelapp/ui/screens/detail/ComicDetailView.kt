package backhero.marvelapp.ui.screens.detail

import backhero.marvelapp.data.model.Comic
import backhero.marvelapp.ui.base.mvp.BaseView

/**
 * Created by javieralvarez on 28/1/18.
 */
interface ComicDetailView  : BaseView{
    fun showComic(comic : Comic)
    fun showBackButton()
}