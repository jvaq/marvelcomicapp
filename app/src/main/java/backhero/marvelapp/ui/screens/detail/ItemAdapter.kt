package backhero.marvelapp.ui.screens.detail

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import backhero.marvelapp.R
import backhero.marvelapp.data.model.Item
import backhero.marvelapp.utils.RoleMapper
import backhero.marvelapp.utils.extensions.inflate
import org.jetbrains.anko.find

/**
 * Created by javieralvarez on 28/1/18.
 */
class ItemAdapter() : RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {

    var items: MutableList<Item> = mutableListOf()

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) = holder.bind(items[position])

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder = ItemViewHolder(parent.inflate(R.layout.item_items))

    override fun getItemCount(): Int = items.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val comicTitle: TextView = itemView.find(R.id.tv_item_title)
        private val comicDescription: TextView = itemView.find(R.id.tv_item_description)
        private val comicRole: TextView = itemView.find(R.id.tv_item_role)


        fun bind(item: Item) = with(itemView) {
            comicTitle.text = item.name
            comicDescription.text = item.type
            comicRole.text = RoleMapper.generateRol(item.role)
        }
    }

    fun addItems(newComic: List<Item>) {
        items.addAll(newComic)
        notifyDataSetChanged()
    }
}