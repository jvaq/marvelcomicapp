package backhero.marvelapp.ui.screens.main

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import backhero.marvelapp.R
import backhero.marvelapp.data.model.Comic
import backhero.marvelapp.utils.PriceMapper
import backhero.marvelapp.utils.extensions.inflate
import backhero.marvelapp.utils.extensions.loadImage
import backhero.marvelapp.utils.images.ImageGenerator
import org.jetbrains.anko.find

/**
 * Created by javieralvarez on 27/1/18.
 */
class ComicAdapter (private val  moviesListener : ComicListener)  : RecyclerView.Adapter<ComicAdapter.MoviesViewHolder>() {

    var comics : MutableList<Comic> = mutableListOf()

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int)  = holder.bind(comics[position])

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder = MoviesViewHolder(parent.inflate(R.layout.item_comic))

    override fun getItemCount(): Int = comics.size

    inner class MoviesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val comicImage: ImageView = itemView.find(R.id.iv_image)
        private val comicTitle: TextView = itemView.find(R.id.tv_title)
        private val comicDescription: TextView = itemView.find(R.id.tv_overview)
        private val comicPrice: TextView = itemView.find(R.id.tv_price)


        fun bind(comic: Comic) = with(itemView){
            comicImage.loadImage( ImageGenerator.generateImageUrl(comic.images))
            comicTitle.text = comic.title
            comicDescription.text = comic.description
            comic.prices?.first()?.apply { comicPrice.text = PriceMapper.generatePrice(this) }
            setOnClickListener { moviesListener.onComicSelected(comic) }

        }
    }

    fun addComics(newComic : List<Comic>){
        comics.addAll(newComic)
        notifyDataSetChanged()
    }

}