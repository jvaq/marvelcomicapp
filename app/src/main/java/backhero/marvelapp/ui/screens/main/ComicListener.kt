package backhero.marvelapp.ui.screens.main

import backhero.marvelapp.data.model.Comic

/**
 * Created by javieralvarez on 27/1/18.
 */
interface ComicListener {
    fun onComicSelected(comic : Comic)
}