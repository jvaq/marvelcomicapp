package backhero.marvelapp.ui.screens.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import backhero.marvelapp.R
import backhero.marvelapp.data.model.Comic
import backhero.marvelapp.handlers.NavigationHandler
import backhero.marvelapp.ui.base.views.BaseActivity
import backhero.marvelapp.ui.screens.views.InfiniteScrollListener
import backhero.marvelapp.ui.screens.views.MarvelNavbar
import backhero.marvelapp.utils.extensions.navigateActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), MainView , ComicListener{


    override val layout: Int = R.layout.activity_main
    lateinit var marvelNavbar : MarvelNavbar
    lateinit var rvComics : RecyclerView
    val presenter : MainPresenter = MainPresenter(this)
    val adapter = ComicAdapter(this)


    override fun bindViews() {
        marvelNavbar = navbar
        rvComics = rv_comics
    }

    override fun loadData() {
        presenter.init()
        rvComics.adapter = adapter
        val linearLayout = LinearLayoutManager(this)
        rvComics.layoutManager = linearLayout
        rvComics.addOnScrollListener(InfiniteScrollListener(linearLayout, { presenter.loadMoreComics() }))
    }

    override fun addComics(comics: List<Comic>) {
        adapter.addComics(comics)
    }

    override fun showError(message: String) {
    }

    override fun onComicSelected(comic: Comic) {
        var bundle = Bundle()
        bundle.putString("id", comic.id.toString())
        this.navigateActivity(NavigationHandler.ActivityDestinations.COMIC_DETAIL,bundle)
    }


}
