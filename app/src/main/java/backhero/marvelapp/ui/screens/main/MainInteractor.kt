package backhero.marvelapp.ui.screens.main

import backhero.marvelapp.data.model.Comic
import backhero.marvelapp.data.model.Response
import backhero.marvelapp.handlers.DataHandler
import backhero.marvelapp.ui.base.mvp.BaseInteractor
import io.reactivex.Observable

/**
 * Created by javieralvarez on 27/1/18.
 */
class MainInteractor : BaseInteractor() {

    fun getComicsByCharacter(offset : Int) : Observable<Response<Comic>>{
        val response = DataHandler.getInstance().comicHandler.getComicsByCharacter("1009368",offset)
        return response
    }
}