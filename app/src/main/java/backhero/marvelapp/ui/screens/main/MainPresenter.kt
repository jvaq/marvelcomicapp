package backhero.marvelapp.ui.screens.main

import backhero.marvelapp.data.model.Comic
import backhero.marvelapp.ui.base.mvp.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by javieralvarez on 27/1/18.
 */
class MainPresenter(mView: MainView) : BasePresenter<MainView, MainInteractor>(mView) {

    override val interactor: MainInteractor = MainInteractor()
    var offset: Int = 0

    override fun init() {
        interactor.getComicsByCharacter(offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            it.data?.results?.let { addComics(it) }
                        },
                        {
                            it.message?.let { it1 -> mView?.showError(it1) }
                        })
    }

    fun loadMoreComics() {

        interactor.getComicsByCharacter(offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    it.data?.results?.let {
                        addComics(it)
                    }
                }
    }

    private fun addComics(comics: List<Comic>) {
        offset += comics.size
        mView?.addComics(comics)
    }
}