package backhero.marvelapp.ui.screens.main

import backhero.marvelapp.data.model.Comic
import backhero.marvelapp.ui.base.mvp.BaseView

/**
 * Created by javieralvarez on 27/1/18.
 */
interface MainView  : BaseView {

    fun addComics(comics : List<Comic>)
    fun showError(message : String)
}