package backhero.marvelapp.ui.screens.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import backhero.marvelapp.R
import kotlinx.android.synthetic.main.marvel_navbar.view.*
import org.jetbrains.anko.dimen

/**
 * Created by javieralvarez on 27/1/18.
 */
class MarvelNavbar @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : RelativeLayout(context, attrs, defStyleAttr) {

    private val ivBackButton: ImageView
    private val tvTitle : TextView

    init {
        View.inflate(context, R.layout.marvel_navbar, this)
        ivBackButton = back_bt
        tvTitle = tv_title


    }
    fun showBackButton(show : Boolean){
        if (show) ivBackButton.visibility = View.VISIBLE
        else ivBackButton.visibility = GONE
    }

    fun onBackPressed(listener : View.OnClickListener){
        ivBackButton.setOnClickListener(listener)
    }

    fun changeTitle(title : String){
        tvTitle.text = title
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, View.MeasureSpec.makeMeasureSpec(dimen(R.dimen.toolbar_height),
                View.MeasureSpec.EXACTLY))
    }
}