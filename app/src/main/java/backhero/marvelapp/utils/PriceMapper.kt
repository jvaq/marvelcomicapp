package backhero.marvelapp.utils

import backhero.marvelapp.data.model.Price

/**
 * Created by javieralvarez on 28/1/18.
 */
class PriceMapper {
    companion object {

        fun generatePrice(price: Price): String {
            return  price.price.toString() + " $"
        }
    }
}