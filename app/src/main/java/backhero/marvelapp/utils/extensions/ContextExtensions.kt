package backhero.marvelapp.utils.extensions

import android.content.Context
import android.content.Intent
import android.os.Bundle
import backhero.marvelapp.handlers.NavigationHandler

/**
 * Created by javieralvarez on 28/1/18.
 */

fun Context.navigateActivity(activityDestination : String,bundle: Bundle?){
    var intent = Intent(this,    NavigationHandler.getActivityDestination(activityDestination))
    bundle?.let { intent.putExtras(bundle) }
    startActivity(intent)
}