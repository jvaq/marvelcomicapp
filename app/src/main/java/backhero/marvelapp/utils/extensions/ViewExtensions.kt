package backhero.marvelapp.utils.extensions

import android.support.annotation.DrawableRes
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import backhero.marvelapp.handlers.ImageHandler

/**
 * Created by javieralvarez on 27/1/18.
 */

fun ViewGroup.inflate(
        @LayoutRes layoutRes: Int,
        attachToRoot: Boolean = false): View {

    return LayoutInflater
            .from(context)
            .inflate(layoutRes, this, attachToRoot)
}

fun ImageView.loadImage(url: String?, @DrawableRes errorPlaceholder: Int = 0) {
    if (url != null) ImageHandler.instance.loadImage(context, url, this, errorPlaceholder)
    else this.setImageResource(errorPlaceholder)
}
