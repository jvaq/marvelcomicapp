package backhero.marvelapp.utils.images

import backhero.marvelapp.data.model.MImages

/**
 * Created by javieralvarez on 28/1/18.
 */
class ImageGenerator {

    companion object {

        fun generateImageUrl(images: List<MImages>?): String {
            var url = ""
            if (images != null && !images.isEmpty()) {
                images.first().apply {
                    url =  this.path + "/portrait_uncanny." + this.extension
                }
            }
            return url
        }

        fun generateLandscapeImageUrl(images: List<MImages>?): String {
            var url = ""
            if (images != null && !images.isEmpty()) {
                images.first().apply {
                    url =  this.path + "/landscape_incredible." + this.extension
                }
            }
            return url
        }
    }
}