package backhero.marvelapp.utils.images

import android.content.Context
import android.support.annotation.DrawableRes
import android.widget.ImageView

/**
 * Created by javieralvarez on 28/1/18.
 */
interface ImageProvider {

    fun loadImage(context: Context, url: String, imageView: ImageView): ImageRequest

    fun loadImage(context: Context, url: String, imageView: ImageView, @DrawableRes errorHolder: Int): ImageRequest

}